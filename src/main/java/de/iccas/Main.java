package de.iccas;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import de.iccas.backend.database.DataAccessController;
import de.iccas.backend.file.GeNIeFileReader;
import de.iccas.backend.model.ExaminationGroup;
import de.iccas.backend.model.Patient;
import de.iccas.backend.util.Log;
import de.iccas.backend.util.Preferences;
import de.iccas.ui.ExamView;
import de.iccas.ui.NeckView;
import de.iccas.ui.StartView;

import javax.servlet.annotation.WebServlet;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Random;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 *
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 *
 * @author Chris Unger, ICCAS, 2018
 */
@Theme("mytheme")
public class Main extends UI {

    /**
     * The preferences.
     */
    public Preferences preferences;

    /**
     * Wheter to use database and file at the same time for redundant saving.
     */
    public final boolean USE_DB_AND_FILE = true;

    /**
     * The log.
     */
    public Log log;

    /**
     * The navigator instance to navigate through different views / pages.
     */
    public Navigator navigator;

    /**
     * The main view name.
     */
    public static final String MAINVIEW = "main";

    /**
     * The examination view name.
     */
    public static final String EXAMVIEW = "exam";

    /**
     * The genie file reader.
     */
    public GeNIeFileReader reader;

    /**
     * The list containing all patients. If a patient is added be sure to save to the file as well.
     */
    private LinkedList<Patient> patients;

    /**
     * The list containing all available groups of examinations.
     * This list shouldn't change after initial parsing of the file.
     */
    private LinkedList<ExaminationGroup> examinations;

    /**
     * The index of the currently viewed patient.
     */
    private int currentPatientIndex;

    public DataAccessController database;


    /**
     * The method which initializes the web application.
     * @param request
     */
    @Override
    protected void init(VaadinRequest request) {
        preferences = new Preferences();
        if(preferences.isLoggingEnabled()) {
            log = new Log(preferences.getLogFile());
        }

        getPage().setTitle("Exam Tool for DigitalPatientModel");
        log.info("Starting tool...");

        log.info("Initializing GeNIeFileReader...");
        reader = new GeNIeFileReader(preferences.getGenieFile(), this);

        log.info("Getting list of examination groups.");
        examinations = reader.readFileAndGetExamGroups();
//        for(ExaminationGroup e : examinations) {
//            System.out.println(e.getExaminations());
//        }

        if(preferences.usesDataBase()) {
            log.info("Database usage enabled:");
            log.info("  Initializing database connection...");
            database = new DataAccessController(this, preferences.getDataBaseURL());
            //read patients from database -> TODO only load current one?
            log.info("  Reading patients from database...");
            patients = database.getPatients();
            log.info("Finished database initialization!");
        } else {
            log.info("Database usage disabled: Skipping initialization!");
            log.info("Loading patients...");
            patients = reader.getPatientsFromFile();
        }

        //set the current patient to the last one
        currentPatientIndex = patients.size() - 1;

        log.info("Initializing view...");
        // Create a navigator to control the views
        navigator = new Navigator(this, this);

        // Create and register the views
        navigator.addView("", new StartView(this));
        NeckView neckView = new NeckView(this);
        navigator.addView(MAINVIEW, neckView);
        navigator.addView(EXAMVIEW, new ExamView(this, neckView));
        log.info("Initialization completed! Ready for usage.\n===========================================");
    }

    /**
     * Gets the list of available patients.
     * @return
     */
    public LinkedList<Patient> getPatients() {
        return patients;
    }

    /**
     * Gets the list of available examinations.
     * @return
     */
    public LinkedList<ExaminationGroup> getExaminations() {
        return examinations;
    }

    /**
     * Gets the index of the currently viewed patient.
     * @return
     */
    public int getCurrentPatientIndex() {
        return currentPatientIndex;
    }

    /**
     * Change current patient to the patient with the given ID
     * @param patID The ID of the patient to switch to.
     * @return true of successful; false else
     */
    public boolean changeToPatient(int patID) {
        if(patID == currentPatientIndex) {
            //if the patient with the given id is already showing then nothing has to be done
            return true;
        }
        int oldIndex = currentPatientIndex;
        for(Patient p : patients) {
            if(p.getID() == patID) {
                //set the index of the current patient to the one with the id
                currentPatientIndex = patients.indexOf(p);
                return true;
            }
        }
        return false;
    }

    /**
     * Switches to the next patient.
     */
    public void nextPatient() {
        saveCurrentPatient();
        //add a new patient if next was pressed after last patient
        if(currentPatientIndex >= patients.size() - 1) {
            patients.add(new Patient(getNewPatientID()));
            //set fake information if enabled
            if(Preferences.FAKE_MODE) {
                patients.getLast().setName("Patient", ""+patients.getLast().getID());
                patients.getLast().setBirthDate(LocalDate.of(1960,1,1));
            }
        }
        currentPatientIndex++;
    }

    /**
     * Switches to previous patient.
     */
    public void prevPatient() {
        saveCurrentPatient();
        currentPatientIndex--;
    }

    /**
     * Removes the current patient from the patients list and switches to the last patient.
     */
    public void removeCurrent() {
        patients.remove(getCurrentPatient());
        currentPatientIndex--;
    }

    /**
     * Returns the current patient. Adds a new patient if there isn't any
     * @return
     */
    public Patient getCurrentPatient() {
        Patient currentPatient;
        if(currentPatientIndex>-1) {
            currentPatient = patients.get(currentPatientIndex);
        } else {
            //if there isn't any patient add a new one
            currentPatient = new Patient(getNewPatientID());
            currentPatientIndex = 0;
            patients.add(currentPatient);
        }
        return currentPatient;
    }

    /**
     * Generates a 5 digit random number as an unique id for the patient.
     * @return
     */
    public int getNewPatientID() {
        Random rand = new Random();
        int id = rand.nextInt(99999);
        if(id < 10000) {
            return getNewPatientID();
        } else {
            for(Patient p : patients) {
                if(p.getID() == id) {
                    return getNewPatientID();
                }
            }
        }
        return id;
    }

    /**
     * Saves the current patient to file.
     */
    public void saveCurrentPatient() {
        if(preferences.usesDataBase()) {
            if(USE_DB_AND_FILE) {
                reader.savePatient(getCurrentPatient());
            }
            //insert only if new patient, else update procedures and findings
            if(database.getPatientByID(getCurrentPatient().getID()).equals("[]")) {
                database.insertPatient(getCurrentPatient());
            } else {
                //update existing patient...
                //add additional exam groups / procedures
                for(ExaminationGroup eg : getCurrentPatient().getExamGroups()) {
                    database.addProcedure(getCurrentPatient().getID(), eg);
                    //examinations / findings will be added with procedure
                }
            }
        } else {
            reader.savePatient(getCurrentPatient());
        }
    }

    @WebServlet(urlPatterns = "/*", name = "ExamToolServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = Main.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}

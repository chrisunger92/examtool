package de.iccas.backend.util;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Saves the user preferences and gives central access to them.
 * @author Chris Unger, ICCAS, 2016-2018
 */
public class Preferences {

    /**
     * The preferences file.
     */
    private File preferencesFile;

    /**
     * The genie file.
     */
    private File genieFile;

    /**
     * The log file.
     */
    private File logFile;

    /**
     * Whether logging is enabled or not.
     */
    private boolean loggingEnabled;

    /**
     * Whether to use the database (true) for patient data or the genie file (false).
     */
    private boolean useDataBase;

    /**
     * The URL to the database.
     */
    private String dataBaseURL;

    private String dataBaseUser;

    private String dataBasePW;

    private boolean fakeMode;

    /**
     * The default value of loggingEnabled.
     */
    private final boolean DEFAULT_LOGGING_ENABLED = true;

    /**
     * The preferences file as a String.
     */
    private String fileAsSting;

    /**
     * The default genie file path.
     */
    private final String DEFAULT_GENIE_PATH
            = "test_xmls/validationPaper_TNM-Model_28012016_anonym.xdsl";
//            = "test_xmls/genieModel_kleineStudie_korr.xdsl";
//            = "test_xmls/validationPaper_TNM-Model_28012016.xdsl";

    /**
     * The default log file path.
     */
    private final String DEFAULT_LOG_PATH = "logs/latest.log";

    /**
     * Whether to use real patient information or fake names and date of birth.
     */
    public static final boolean FAKE_MODE = true;

    /**
     * The default database URL
     */
    private final String DATA_BASE_URL = "jdbc:mysql://10.50.5.63:3306/dpm_larynx";
//                        "jdbc:mysql://localhost:3306/dpm_larynx?&serverTimezone=UTC";

    private final String DATABASE_USER = "examtool"/*"root"*/;

    private final String DATABASE_PW = "extool42-$13"/*"sql42-1$"*/;

    /**
     * Whether to use the database or the genie file.
     */
    private final boolean USE_DATABASE = true;

    public static final String UPLOAD_PATH = "upload";


    /**
     * Constructor
     * @param preferencesFile The file to save the preferences to.
     */
    public Preferences(File preferencesFile) {
        this.preferencesFile = preferencesFile;
        if(preferencesFile.exists() && !preferencesFile.isDirectory()) {
//            System.out.println("Found preferences in file: " + preferencesFile.getAbsolutePath());
            readPreferencesFile();
        } else {
//            System.out.println("Initializing new preferences in file: " + preferencesFile.getAbsolutePath());
            initPrefFile();
        }
    }

    /**
     * Constructor
     */
    public Preferences() {
        this.preferencesFile = new File("preferences.conf");
        if(preferencesFile.exists() && !preferencesFile.isDirectory()) {
            readPreferencesFile();
        } else {
            initPrefFile();
        }
    }

    /**
     * This is called the first time a preference file is created.
     */
    private void initPrefFile() {
        resetPrefsToDefaultValues();
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(preferencesFile.getPath()), "utf-8"))) {
            //genie file not specified yet
            writer.write("genieFile=" + DEFAULT_GENIE_PATH + ";\n");
            //disable logging by default
            writer.write("enableLogging=" + DEFAULT_LOGGING_ENABLED + ";\n");
            writer.write("logFile=" + DEFAULT_LOG_PATH + ";\n");
            writer.write("useDataBase=" + USE_DATABASE + ";\n");
            writer.write("dataBaseURL=" + DATA_BASE_URL + ";\n");
            writer.write("dataBaseUser=" + DATABASE_USER + ";\n");
            writer.write("dataBasePW=" + DATABASE_PW+ ";\n");
            writer.write("fakeMode=" + FAKE_MODE + ";\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Resets the preferences to default values.
     */
    public void resetPrefsToDefaultValues() {
        genieFile = new File(DEFAULT_GENIE_PATH);
        loggingEnabled = DEFAULT_LOGGING_ENABLED;
        useDataBase = USE_DATABASE;
        dataBaseURL = DATA_BASE_URL;
        dataBaseUser = DATABASE_USER;
        dataBasePW = DATABASE_PW;
        logFile = new File(DEFAULT_LOG_PATH);
        fakeMode = FAKE_MODE;
    }

    /**
     * Saves preferences to file.
     */
    public void savePreferences() {
        List<String> lines = Arrays.asList(
                "genieFile=" + genieFile.getAbsolutePath() + ";",
                "enableLogging=" + loggingEnabled + ";",
                "logFile=" + logFile.getAbsolutePath() + ";",
                "useDataBase=" + useDataBase + ";",
                "dataBaseURL=" + dataBaseURL + ";",
                "dataBaseUser=" + dataBaseUser + ";",
                "dataBasePW=" + dataBasePW + ";",
                "fakeMode=" + fakeMode + ";"
        );
        Path file = Paths.get(preferencesFile.getPath());
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads preferences from file.
     */
    public void readPreferencesFile() {
        try(BufferedReader br = new BufferedReader(new FileReader(preferencesFile.getPath()))) {
            //first read preferences file and put everything into a string
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            fileAsSting = sb.toString();
            //read genie file preferences
            if(fileAsSting.contains("genieFile=")) {
                int start = fileAsSting.indexOf("genieFile=") + 10;//10 is the length of "genieFile=" - we dont want that part
                int end = fileAsSting.indexOf(";", start);
                genieFile = new File(fileAsSting.substring(start, end));
            }
            if(fileAsSting.contains("enableLogging=")) {
                int start = fileAsSting.indexOf("enableLogging=") + 14;//14 is the length of "enableLogging=" - we dont want that part
                int end = fileAsSting.indexOf(";", start);
                if(fileAsSting.substring(start, end).equals("true")) {
                    loggingEnabled = true;
                } else {
                    loggingEnabled = false;
                }
            }
            if(fileAsSting.contains("logFile=")) {
                int start = fileAsSting.indexOf("logFile=") + 8;//8 is the length of "logFile=" - we dont want that part
                int end = fileAsSting.indexOf(";", start);
                logFile = new File(fileAsSting.substring(start, end));
            }
            if(fileAsSting.contains("useDataBase=")) {
                int start = fileAsSting.indexOf("useDataBase=") + 12;//21 is the length of "useDataBase=" - we dont want that part
                int end = fileAsSting.indexOf(";", start);
//                System.out.println("useDatabase=" + fileAsSting.substring(start, end));
                if(fileAsSting.substring(start, end).equals("true")) {
                    useDataBase = true;
                } else {
                    useDataBase = false;
                }
            }
            if(fileAsSting.contains("dataBaseURL=")) {
                int start = fileAsSting.indexOf("dataBaseURL=") + 12;//12 is the length of "dataBaseURL=" - we dont want that part
                int end = fileAsSting.indexOf(";", start);
                dataBaseURL = fileAsSting.substring(start, end);
            }
            if(fileAsSting.contains("dataBaseUser=")) {
                int start = fileAsSting.indexOf("dataBaseUser=") + 13;//13 is the length of "dataBaseUser=" - we dont want that part
                int end = fileAsSting.indexOf(";", start);
                dataBaseUser = fileAsSting.substring(start, end);
            }
            if(fileAsSting.contains("dataBasePW=")) {
                int start = fileAsSting.indexOf("dataBasePW=") + 11;//11 is the length of "dataBasePW=" - we dont want that part
                int end = fileAsSting.indexOf(";", start);
                dataBasePW = fileAsSting.substring(start, end);
            }
            if(fileAsSting.contains("fakeMode=")) {
                int start = fileAsSting.indexOf("fakeMode=") + 9;//9 is the length of "useDataBase=" - we dont want that part
                int end = fileAsSting.indexOf(";", start);
//                System.out.println("useDatabase=" + fileAsSting.substring(start, end));
                if(fileAsSting.substring(start, end).equals("true")) {
                    fakeMode = true;
                } else {
                    fakeMode = false;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the value of the local variable 'genieFile'.
     * @param genieFile
     */
    public void setGenieFile(File genieFile) {
        this.genieFile = genieFile;
    }

    /**
     * Returns the GeNIe file.
     * @return GeNIe file
     */
    public File getGenieFile() {
        return genieFile;
    }

    /**
     * Returns the log file.
     * @return
     */
    public File getLogFile() {
        return logFile;
    }

    /**
     * Sets the log file.
     * @param logFile
     */
    public void setLogFile(File logFile) {
        this.logFile = logFile;
    }

    /**
     * Gets whether logging is enabled.
     * @return
     */
    public boolean isLoggingEnabled() {
        return loggingEnabled;
    }

    /**
     * Sets the value of loggingEnabled.
     * @param loggingEnabled true - logging enabled
     *                       false - logging disabled
     */
    public void setLoggingEnabled(boolean loggingEnabled) {
        this.loggingEnabled = loggingEnabled;
    }

    /**
     *
     * @return
     */
    public boolean usesFakeMode() {
        return fakeMode;
    }

    /**
     *
     * @param use
     */
    public void setFakeMode(boolean use) {
        fakeMode = use;
    }

    /**
     * Whether database use is enabled or disabled.
     * @return
     */
    public boolean usesDataBase() {
        return useDataBase;
    }

    /**
     * Enable or disable use of database.
     * @param useDataBase
     */
    public void setUseDataBase(boolean useDataBase) {
        this.useDataBase = useDataBase;
    }

    /**
     * The database URL.
     * @return
     */
    public String getDataBaseURL() {
        return dataBaseURL;
    }

    /**
     * Set the database URL
     * @param dataBaseURL
     */
    public void setDataBaseURL(String dataBaseURL) {
        this.dataBaseURL = dataBaseURL;
    }

    /**
     *
     * @return
     */
    public String getDatabaseUser() {
        return dataBaseUser;
    }

    /**
     *
     * @param userName
     */
    public void setDataBaseUser(String userName) {
        dataBaseUser = userName;
    }

    /**
     *
     * @return
     */
    public String getDatabasePW() {
        return dataBasePW;
    }

    /**
     *
     * @param pw
     */
    public void setDataBasePW(String pw) {
        dataBasePW = pw;
    }

    /**
     * Writes the preference file as a string.
     * @return The String representing the preferences file.
     */
    @Override
    public String toString() {
        return fileAsSting;
    }
}

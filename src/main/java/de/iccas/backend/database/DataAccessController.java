package de.iccas.backend.database;

import de.iccas.Main;
import de.iccas.backend.model.Examination;
import de.iccas.backend.model.ExaminationGroup;
import de.iccas.backend.model.Findings;
import de.iccas.backend.model.Patient;
import de.iccas.backend.util.Log;
import de.iccas.backend.util.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.*;
import java.time.LocalDate;
import java.util.LinkedList;

public class DataAccessController {

    private Connection con = null;

    private final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";

    private final String DATABASEURL;

    //TODO maybe add to preferences
    /**
     * The date to start considering patients in the database.
     */
    private final String STARTDATE = "'2018-04-01'";

    // ==================== QUERIES =========================
    //patient queries are limited by date, only patients since 1st of April 2018 are considered
    private final String GET_ALL_PATIENT_IDS = "SELECT patientID FROM patients WHERE patientCreationDate > " +
            STARTDATE;
    private final String GET_PATIENT_COUNT = "SELECT COUNT(patientID) AS numPatients FROM patients " +
            "WHERE patientCreationDate > " + STARTDATE;
    //these use the date as well to ensure uniqueness of patientID
    private final String GET_PATIENT_BY_ID_FULL = "SELECT * FROM patients WHERE patientCreationDate > " +
            STARTDATE + "' AND patientID = ";
    private final String GET_PATIENT_BY_ID_LITE = "SELECT patientID, first_name, last_name, birthdate " +
            "FROM patients WHERE patientCreationDate > " + STARTDATE + " AND patientID = ";
    private final String GET_TNM_BY_PATIENT_ID = "SELECT * FROM tnmstaging WHERE patientID = ";
    private final String GET_PROCEDURES_BY_PATIENT_ID = "SELECT id,procedurecode,proceduretext,proceduredate " +
            "FROM procedures WHERE patientID = ";
    private final String GET_FINDINGS_BY_PROCEDURE_ID = "SELECT findings FROM findings WHERE procedureID = ";
    private final String GET_PROCEDURES_WITH_FINDINGS_BY_PATIENT_ID = "SELECT * " +
            "FROM " +
                "(SELECT patientID, procedureDate, procedureCode, procedureText, findings " +
                "FROM procedures p , findings f " +
                "WHERE f.procedureID = p.id) AS procFindings" +
            "WHERE patientID =";
    private final String GET_LAST_ADDED_PROCEDURE_ID = "SELECT MAX(ID) FROM procedures";
    private final String GET_TUMORBOARD_DATE_BY_PATIENT_ID = "SELECT tbDate FROM tumorboards WHERE patID = ";
    // ======================================================

    // ================ COLUMN / VARIABLE NAMES =============
    private final String PAT_ID = "patientID";
    private final String PAT_FIRST_NAME = "first_name";
    private final String PAT_LAST_NAME = "last_name";
    private final String PAT_BIRTHDATE = "birthdate";
    private final String PAT_CREATION_DATE = "patientCreationDate";

    private final String PROC_TEXT = "proceduretext";
    private final String PROC_CODE = "procedurecode";
    private final String PROC_DATE = "proceduredate";
    // ======================================================

    private Log log;

    private Main main;

    /**
     * Constructor.
     * @param mainInstance The current instance of the main class (Main).
     */
    public DataAccessController(Main mainInstance, String dataBaseURL) {
        this.main = mainInstance;
        DATABASEURL = dataBaseURL;
        log = main.log;
    }

    /**
     * Initializes the database.
     */
    private void initDB() {
        try {
            Class.forName(JDBC_DRIVER);
            log.info("Connecting to specified database: " + DATABASEURL + "...");
            con = DriverManager.getConnection(DATABASEURL,
                    main.preferences.getDatabaseUser(), main.preferences.getDatabasePW());
            log.info("DB connection established!");
        } catch (SQLException e) {
            log.error(e.getMessage());
            log.error("SQL state: " + e.getSQLState());
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Executes a query. (only SELECT)
     * @param query The query to execute.
     * @return
     */
    private String executeQuery(String query) {

        if (query == null) {
            return null;
        }

        if (con == null) {
            initDB();
        }

        JSONArray jsonArray = new JSONArray();
        try {
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);

            while (resultSet.next()) {
                int total_rows = resultSet.getMetaData().getColumnCount();
                for (int i = 0; i < total_rows; i++) {
                    JSONObject obj = new JSONObject();
                    obj.put(resultSet.getMetaData().getColumnLabel(i + 1)
                            .toLowerCase(), resultSet.getObject(i + 1));
                    jsonArray.put(obj);
                }
            }
            stmt.close();

        } catch (SQLException e) {
            log.error(e.getMessage() + "\n  QUERY: " + query);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray.toString();
    }

    /**
     * Executes a update query. (INSERT, UPDATE, DELETE)
     * @param query The update query to execute.
     * @return number of rows affected by update query
     */
    private int executeUpdate(String query) {

        if (query == null) {
            return 0;
        }

        if (con == null) {
            initDB();
        }

        int result = 0;

        try {
            Statement stmt = con.createStatement();
            result = stmt.executeUpdate(query);
            stmt.close();

        } catch (SQLException e) {
            log.error(e.getMessage() + "\n  QUERY: " + query);
        }

        return result;
    }

    /**
     * Insert a patient into the database.
     * CHECK FOR EXISTING PATIENT BEFOREHAND!
     * @param patient
     */
    public void insertPatient(Patient patient) {
        //insert patient into patient table
        // patient: patientID, first_name, last_name, birthdate, patientCreationDate
        String patientQuery = "INSERT INTO patients VALUES(";
        patientQuery += patient.getID() + ",";
        patientQuery += "'" + patient.getFirstName() + "',";
        patientQuery += "'" + patient.getLastName() + "',";
        patientQuery += "'" + patient.getBirthDate() + "',";
        patientQuery += "'" + LocalDate.now() +"'";
        patientQuery += ")";
        //execute query and write output to log
        int executed = executeUpdate(patientQuery);
        if(executed > 0) {
            log.info("Updated table patients by inserting a new patient.");
        } else {
            log.error("Could not insert patient!");
        }

        //insert examinations into procedures table
        int numProceduresAdded = 0;
        for(ExaminationGroup eg : patient.getExamGroups()) {
            if(addProcedure(patient.getID(), eg)) {
                numProceduresAdded++;
            }
        }
        log.info("Inserted/updated " + numProceduresAdded + " rows into/from table procedures.");
    }

    /**
     * Inserts or updates a procedure and the containing findings.
     * @param patientID
     * @param proc
     */
    public boolean addProcedure(int patientID, ExaminationGroup proc) {
        boolean success = false;
        String checkQuery = "SELECT id FROM procedures WHERE patientID=" + patientID + " AND procedureDate='"
                + proc.getDate() + "' AND procedureText='" + proc.getName() + ": " + proc.getPlaceOfExam() + "'";
        String id = executeQuery(checkQuery);
        if(id.equals("[]")) {
            //insert procedure if not existing
            String procQuery = "INSERT INTO procedures (patientID,procedureDate,procedureCode,procedureText) VALUES(";
            procQuery += patientID + ",";
            procQuery += "'" + proc.getDate() + "',";
            //TODO get correct code and use that
            procQuery += "'" + "',";
            //procedure text -> ExamGrp name and place for now: could be used to save additional floating text notes
            procQuery += "'" + proc.getName() + ": " + proc.getPlaceOfExam() + "'";
            procQuery += ")";
            if(executeUpdate(procQuery) > 0) {
                success = true;
            }
            //update id -> get id of added procedure
            id = executeQuery("SELECT MAX(id) AS lastIndex FROM procedures");
            log.info("Added new procedure with id=" + id + " into table procedures!");
        } else {
            //TODO update procedure if existing
        }
        id = id.substring(id.indexOf(":") + 1, id.indexOf("}"));
        //insert findings into findings table
        int numFindingsAdded = 0;
        for(Examination e : proc.getExaminations()) {
            if(addFindings(id, e)) {
                numFindingsAdded++;
            }
        }
        log.info("Inserted " + numFindingsAdded + " rows into table findings.");
        return success;
    }

    /**
     *
     * @param procedureID
     * @param ex
     * @return
     */
    private boolean addFindings(String procedureID, Examination ex) {
        boolean success = false;
        //Check for existing findings with same content
        String checkQuery = "SELECT id FROM findings WHERE procedureID=" + procedureID + " AND " +
                "findings='" + ex.getSQLString() + "'";
        //TODO add check for exam name only, to enable updating values...
        String id = executeQuery(checkQuery);
        if(id.equals("[]")) {
            //insert findings as it is not existing
            String findQuery = "INSERT INTO findings (findings, procedureID) VALUES(";
            findQuery += "'" + ex.getSQLString() + "',";
            findQuery += procedureID;
            findQuery += ")";
            if(executeUpdate(findQuery) > 0) {
                success = true;
            }
        } else {
            //TODO update existing findings with new values
        }
        return success;
    }

    /**
     * Updates an existing patient with all of its procedures and findings.
     * @param patient
     */
    public void updatePatient(Patient patient) {
        //TODO update patient with its procedures (examgrps) and findings
    }

    /**
     * Gets patient data by id
     * @param id The patients id
     * @return Patient data or null if not found
     */
    public String getPatientByID(int id) {
        return executeQuery(GET_PATIENT_BY_ID_LITE + id);
    }

    /**
     * Gets the patient from the database with all of its examinations and findings.
     * @return the patient
     */
    //TODO add check whether patient id was found...
    public Patient getPatient(int id) {
        log.info("  Searching for patient with id=" + id + " in database...");
        Patient patient = new Patient(id);
        String patientDataString = executeQuery(GET_PATIENT_BY_ID_LITE + id);
//        System.out.println(patientDataString);
        int start = patientDataString.indexOf(PAT_FIRST_NAME) + PAT_FIRST_NAME.length() + 3;
        int end = patientDataString.indexOf("\"", start);
        patient.setFirstName(patientDataString.substring(start, end));
        start = patientDataString.indexOf(PAT_LAST_NAME) + PAT_LAST_NAME.length() + 3;
        end = patientDataString.indexOf("\"", start);
        patient.setLastName(patientDataString.substring(start, end));
        start = patientDataString.indexOf(PAT_BIRTHDATE) + PAT_BIRTHDATE.length() + 3;
        end = patientDataString.indexOf("\"", start);
        patient.setBirthDate(LocalDate.parse(patientDataString.substring(start, end)));
        //initialize progress
        patient.getProgress().setNumExams(main.reader.getNumExamsPerGroup());
        //read ExaminationGroups / procedures
        readExaminationGroups(patient);

        //Check whether patient was already part of a tumor board
        log.info("  Checking for tumor board entries...");
        String tumorBoardDate = executeQuery(GET_TUMORBOARD_DATE_BY_PATIENT_ID + id);
        if(!tumorBoardDate.equals("[]")) {
            //[{"tbdate":"2018-04-01"}]
            patient.setTumorBoardDate(LocalDate.parse(tumorBoardDate.substring(tumorBoardDate.indexOf("tbdate") + 9,
                    tumorBoardDate.indexOf("}") - 1)));
            log.info("  Found an entry! Informing the user...");
        }

        log.info("  Complete!");
//        System.out.println(patient);
        return patient;
    }

    /**
     * Reads the procedures aka examination groups
     * Example:
     * [{"id":1},{"procedurecode":""},{"proceduretext":"Clinical Examination: internal"},{"proceduredate":"2017-07-04"}, ... ]
     *
     * @param patient
     */
    private void readExaminationGroups(Patient patient) {
        log.info("    Reading procedures...");
        String proceduresString = executeQuery(GET_PROCEDURES_BY_PATIENT_ID + patient.getID());
        log.info("    Found: " + proceduresString);
        int numProcs = 0;
        while(proceduresString.contains("\"id\"")) {
            //procid
            int start = proceduresString.indexOf("id") + 4;
            int end = proceduresString.indexOf("}", start);
            String procID = proceduresString.substring(start, end);
            log.info("    Reading procedure with id: " + procID);
            //TODO procedurecode

            //proceduretext
            start = proceduresString.indexOf(PROC_TEXT) + PROC_TEXT.length() + 3;
            end = proceduresString.indexOf("\"", start);
            String proceduretext = proceduresString.substring(start, end);
            //proceduredate
            start = proceduresString.indexOf(PROC_DATE) + PROC_DATE.length() + 3;
            end = proceduresString.indexOf("\"", start);
            LocalDate procDate = LocalDate.parse(proceduresString.substring(start, end));
            //add examination group
            String examGrpName = proceduretext.substring(0, proceduretext.indexOf(":"));
            log.info("    Found " + examGrpName);
            String examGrpPlace = proceduretext.substring(proceduretext.indexOf(":") + 2);
            ExaminationGroup examGrp = new ExaminationGroup(examGrpName, procDate, examGrpPlace);
            //get and add findings aka examinations
            readExaminations(examGrp, procID, patient);
            patient.addExamGrp(examGrp);
            //remove "row" from string
            proceduresString = proceduresString.substring(end + 3);
            numProcs++;
        }
        log.info("    Added " + numProcs + " procedures aka examination groups to the patient with id=" + patient.getID());
    }

    /**
     * Read examinations aka findings from the database and add them to the examination group
     * Example:
     * [{"findings":"larynx_function_vocal_fold_clinical_examination__patient:normal:0.85"}, ... ]
     *
     * @param examGrp The examination group that will be assigned to the examinations.
     * @param procID The id of the examination group / procedure in the database.
     */
    private void readExaminations(ExaminationGroup examGrp, String procID, Patient patient) {
        log.info("      Searching for findings...");
        int examGrpIndex = Util.getExamGroupIndexByName(examGrp.getName());
        String findingsString = executeQuery(GET_FINDINGS_BY_PROCEDURE_ID + procID);
        int numFindings = 0;
        while(findingsString.contains("\"findings\"")) {
            int start = findingsString.indexOf("findings") + 11;
            int end = findingsString.indexOf("\"", start);
            String findings = findingsString.substring(start, end);
            String name = findings.substring(0, findings.indexOf(":"));
            findings = findings.substring(findings.indexOf(":") + 1); //remove used part
            String outcome = findings.substring(0, findings.indexOf(":"));
            findings = findings.substring(findings.indexOf(":") + 1); //remove used part again
            double certainty = Double.parseDouble(findings.substring(0));
            //add examination to grp with possible outcomes
            ExaminationGroup eg = main.getExaminations().get(Util.getExamGroupIndexByName(examGrp.getName()));
            Examination e = new Examination(name, eg.findByName(name).getPossibleOutcomes().getList(),
                    new Findings(outcome, certainty));
//            System.out.println(e);
            examGrp.addExamination(e);
            //inc progress of matching examination group
            patient.getProgress().incProgress(examGrpIndex);
            numFindings++;
            findingsString = findingsString.substring(end + 2);
        }
        log.info("      Added " + numFindings + " findings aka examinations to the examination group.");
    }

    /**
     * Read all patients from the database.
     * @return The list of existing patients.
     */
    public LinkedList<Patient> getPatients() {
        LinkedList<Patient> patients = new LinkedList<>();
        String numPatientsString = executeQuery(GET_PATIENT_COUNT);
        int numPatients = Integer.parseInt(numPatientsString.substring(numPatientsString.indexOf(":") + 1,
                numPatientsString.indexOf("}")));
        String patientIDs = executeQuery(GET_ALL_PATIENT_IDS);
//        System.out.println(patientIDs);
        //absolute indeces are possible, because length of substrings is always the same
        for(int i = 0; i < numPatients; i++) {
            String patientID = patientIDs.substring(14, 19);
//            System.out.println(patientID);
            patientIDs = patientIDs.substring(20);
//            System.out.println(patientIDs);
            //query for patient with id; add patient to list of patients
            patients.add(getPatient(Integer.parseInt(patientID)));
        }
        return patients;
    }
}
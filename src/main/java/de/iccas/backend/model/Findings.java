package de.iccas.backend.model;

/**
 *
 *
 * @author Chris Unger, ICCAS, 2018
 */
public class Findings {

    private String name;

    private double probability;

    /**
     * Constructor.
     * @param name
     * @param probability
     */
    public Findings(String name, double probability) {
        this.name = name;
        this.probability = probability;
    }

    /**
     * Getter for name.
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for probability.
     * @return
     */
    public double getProbability() {
        return probability;
    }

    /**
     * Setter for probability.
     * @param probability
     */
    public void setProbability(double probability) {
        this.probability = probability;
    }

    @Override
    public String toString() {
        return name + " (certainty: " + probability + ")";
    }
}

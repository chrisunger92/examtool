package de.iccas.backend.model;

import java.time.LocalDate;
import java.util.LinkedList;

/**
 * Class representing an examination group, e.g. MRI, Sono, etc.
 * @author Chris Unger, ICCAS, 2016-2018
 */
public class ExaminationGroup {

    /**
     * The name of the examination.
     */
    private String name;

    /**
     * The date of the examination.
     * This is null for plain (non patient specific) examination groups.
     */
    private LocalDate date;

    /**
     * The place where the examination was done. (internal or external)
     * This is null for plain (non patient specific) examination groups.
     */
    private String placeOfExam;

    /**
     * The list of examinations in this group.
     */
    private LinkedList<Examination> examinations;

    /**
     * Constructor. Use this one for the available examinations.
     * DO NOT USE THIS ONE FOR PATIENT SPECIFIC EXAMINATION GROUPS!
     */
    public ExaminationGroup(String name) {
        this.name = name;
        examinations = new LinkedList<Examination>();
        //initialize with "internal" as default value
        placeOfExam = "internal";
        //initialize with current date
        date = LocalDate.now();
    }

    /**
     * Constructor.
     * @param name The name of the examination group.
     * @param date The date of the examination.
     * @param placeOfExam The place of the examination.
     */
    public ExaminationGroup(String name, LocalDate date, String placeOfExam) {
        this.name = name;
        this.date = date;
        this.placeOfExam = placeOfExam;
        this.examinations = new LinkedList<Examination>();
    }

    /**
     * Constructor.
     * @param name The name of the examination group.
     * @param date The date of the examination.
     * @param placeOfExam The place of the examination.
     * @param examinations The list of examinations in this group.
     */
    public ExaminationGroup(String name, LocalDate date, String placeOfExam, LinkedList<Examination> examinations) {
        this.name = name;
        this.date = date;
        this.placeOfExam = placeOfExam;
        this.examinations = examinations;
    }

    /**
     * Gets the name of the examination group.
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the date the examination was done.
     * @return The date of the examination.
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Gets the place of the examination (internal or external).
     * @return The place of the examination or empty string inf not available
     */
    public String getPlaceOfExam() {
        return placeOfExam!=null?placeOfExam:"";
    }

    /**
     * Adds an examination to the list of examinations if the list doesn't already contain it.
     * @param e The examination to add.
     */
    public void addExamination(Examination e) {
        if(!examinations.contains(e)) {
            examinations.add(e);
        }
    }

    /**
     * Searches the examination list for the given examination name and returns the corresponding examination if
     * found or null if there isn't any with that name.
     * @param name The name of the examination to find.
     * @return The examination corresponding to the name provided.
     */
    public Examination findByName(String name) {
        for(Examination e : examinations) {
            if(e.getName().equals(name)) {
                return e;
            }
        }
        return null;
    }

    /**
     * Returns true if the examination group contains the given examination.
     * @param e The examination to search for.
     * @return true - exam group contains examination
     *         false - exam group does not contain examination
     */
    public boolean contains(Examination e) {
        return examinations.contains(e);
    }

    /**
     * Returns true if the examination group contains the examination with the given name.
     * @param examName The name examination to search for.
     * @return true - exam group contains examination
     *         false - exam group does not contain examination
     */
    public boolean contains(String examName) {
        return findByName(examName) != null;
    }

    /**
     * Gets the whole list of examinations in this group.
     * @return The list of examinations.
     */
    public LinkedList<Examination> getExaminations() {
        return examinations;
    }

    /**
     * Updates the information of the object with another one.
     * @param updatedGroup
     */
    public void update(ExaminationGroup updatedGroup) {
        //TODO update only if it wasn't set?
        this.placeOfExam = updatedGroup.getPlaceOfExam();
        this.date = updatedGroup.getDate();
        for(Examination e : updatedGroup.getExaminations()) {
            //TODO Check whether this is valid, or whether name should be used...
            if(!examinations.contains(e)) {
                examinations.add(e);
            }
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return name + " done " + placeOfExam + " on " + date;
    }

}

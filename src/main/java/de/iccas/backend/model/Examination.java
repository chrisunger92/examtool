package de.iccas.backend.model;

import de.iccas.backend.util.Util;

import java.time.LocalDate;
import java.util.List;

/**
 * Class representing an examination.
 * @author Chris Unger, ICCAS 2016-2018
 */
public class Examination {

    /**
     * The name of the examination.
     */
    private String name;

    /**
     * A list of possible outcomes.
     */
    private PossibleOutcomes possibleOutcomes;

    /**
     * The index of the findings in the list of possible outcomes. This is the outcome, that was entered by the user.
     */
    private Findings findings;

    /**
     * A boolean that indicates whether this examination has a virtual evidence (true) or not (false).
     */
    private boolean hasSoftEvidence;

    /**
     * The state of the examination.
     * (One of Util.UNSURE, Util.TBA, Util.DEFINITE_NO, Util.DONE)
     */
    private String state;

    /**
     * The certainty of the examinations findings.
     */
    private double certainty;

//    /**
//     * The date of the findings.
//     */
//    private LocalDate date;

    /**
     * Constructor. This one should only be used for plain (non patient specific) examinations, i.e. the ones
     * available and listet in the UI.
     * DO NOT USE THIS ONE FOR PATIENT SPECIFIC EXAMINATIONS.
     * @param name
     * @param possibleOutcomes
     */
    public Examination(String name, List<String> possibleOutcomes) {
        this.name = name;
        this.possibleOutcomes = new PossibleOutcomes(possibleOutcomes);
    }

    /**
     * Constructor for examinations without softevidences.
     * @param name Name of the examination.
     * @param possibleOutcomes The list of possible findings.
     * @param findings The findings of the examination.
     */
    public Examination(String name, List<String> possibleOutcomes, Findings findings) {
        this.name = name;
        this.possibleOutcomes = new PossibleOutcomes(possibleOutcomes);
        this.findings = findings;
        this.hasSoftEvidence = false;
    }

    /**
     * Constructor for examinations with softevidences. There are no specified findings if the examination has softevidences.
     * @param name Name of the examination.
     * @param possibleOutcomes The list of possible findings.
     * @param softEvidences The probabilities for all possible findings.
     */
    public Examination(String name, List<String> possibleOutcomes, List<Double> softEvidences) {
        this.name = name;
        this.possibleOutcomes = new PossibleOutcomes(possibleOutcomes);
        this.possibleOutcomes.setProbabilities(softEvidences);
        this.findings = new Findings("softevidences", 0.0);
        this.hasSoftEvidence = true;
    }

    /**
     * Makeshift constructor for findings from database.
     * TODO Use on eof the constructors above and delete this one -> get possible outcomes from GeNIe file...
     * @param name
     * @param findings
     * @param certainty
     */
    public Examination(String name, String findings, double certainty) {
        this.name = name;
        this.findings = new Findings(findings, certainty);
    }

    /**
     * Gets the name of the examination
     * @return The name of the examination.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the specific findings for this examination.
     * DON'T USE THIS IF THERE IS A SOFTEVIDENCE PROVIDED.
     * @return A string with the findings or null if no findings were set
     */
    public Findings getFindings() {
        return findings;
    }

    /**
     * Sets the findings.
     * @param findings
     */
    public void setFindings(Findings findings) {
//        for(Findings f : possibleOutcomes.getFindings()) {
//            if(f.getName().equals(findings.getName())) {
//                f.setProbability(findings.getProbability());
//                this.findings = f;
//                break;
//            }
//        }
        // the above shouldn't be needed, as the provided string should always be a part of the possible outcomes, except
        // it has softevidences
        if(findings.getName().equals("softevidences")) {
            this.findings = new Findings("softevidences", 0.0);
            hasSoftEvidence = true;
        } else {
            this.findings = findings;
        }
    }

    /**
     * @return the hasSoftEvidence
     */
    public boolean hasSoftEvidence() {
        return hasSoftEvidence;
    }

    /**
     * Sets the softevidences
     * @param softEvidences
     */
    public void setSoftEvidences(List<Double> softEvidences) {
        this.hasSoftEvidence = true;
        possibleOutcomes.setProbabilities(softEvidences);
    }

    /**
     *
     * @return
     */
    public PossibleOutcomes getPossibleOutcomes() {
        return possibleOutcomes;
    }

    /**
     *
     * @return list of probabilities if examination has soft evidences; null instead
     */
    public List<Double> getProbabilities() {
        return hasSoftEvidence?possibleOutcomes.getProbabilities():null;
    }

    /**
     * Returns a String with all softevidences in one line divided by spaces.
     * @return
     */
    public String getSoftevidenceString() {
        String str = "";
        for(Double d : possibleOutcomes.getProbabilities()) {
            str += d + " ";
        }
        return str.substring(0, str.length()-1);
    }

    /**
     * Gets the state of the examination.
     * @return The state of the examination.
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the state of the examination. This has to be one of Util.UNSURE, Util.TBA, Util.DEFINITE_NO, Util.DONE
     * @param state
     * @throws IllegalArgumentException if provided state was not found.
     */
    public void setState(String state) {
        if(state.equals(Util.DEFINITE_NO) || state.equals(Util.TBA) || state.equals(Util.UNSURE) || state.equals(Util.DONE)) {
            this.state = state;
        } else {
            throw new IllegalArgumentException("There is now state called " + state + " as a valid possible state!");
        }
    }

//    /**
//     *
//     * @return
//     */
//    public LocalDate getDate() {
//        return date;
//    }
//
//    /**
//     *
//     * @param date
//     */
//    public void setDate(LocalDate date) {
//        this.date = date;
//    }

    /**
     * Gets a string representation used for the database entry
     * name:findings:certainty
     * @return
     */
    public String getSQLString() {
        //TODO Fix issue with TBA exams... (NullPointerException)
        return name + ":" + findings.getName() + ":" + findings.getProbability();
    }

    @Override
    public String toString() {
        String str;
        if(hasSoftEvidence) {
            str = this.name + ": " + findings + "\nPossible findings and softevidences:";
            //+ possibleOutcomes + "\n" + softEvidences;
            for(int i = 0; i < possibleOutcomes.size(); i++) {
                str += "\n" + possibleOutcomes.get(i) + ": " + possibleOutcomes.getProbabilities().get(i);
            }
        } else {
            str = this.name + ": " + findings;
        }
        return str;
    }
}

package de.iccas.backend.model;

import java.util.LinkedList;
import java.util.List;

/**
 * Class for representing the possible outcomes of an examination. This is essentially a list of all possible outcomes
 * with their respective probability.
 *
 * @author Chris Unger, ICCAS, 2018
 */
public class PossibleOutcomes {

    /**
     * The list of possible findings.
     */
    private LinkedList<Findings> findings;


    /**
     * Constructor.
     * @param possibleOutcomes A list of possible outcomes.
     */
    public PossibleOutcomes(List<String> possibleOutcomes) {
        findings = new LinkedList<>();
        for(String s : possibleOutcomes) {
            findings.add(new Findings(s, 1 / (double)(possibleOutcomes.size())));
        }
    }

    /**
     * Constructor.
     * @param possibleOutcomes A list of possible outcomes.
     * @param probabilities A list of probabilities for every possible outcome.
     */
    public PossibleOutcomes(List<String> possibleOutcomes, List<Double> probabilities) {
        findings = new LinkedList<>();
        for(int i = 0; i < possibleOutcomes.size(); i++) {
            findings.add(new Findings(possibleOutcomes.get(i), probabilities.get(i)));
        }
    }

    /**
     * Gets the list of possible outcomes.
     * @return
     */
    public List<String> getList() {
        List<String> possibleOutcomes = new LinkedList<>();
        for(Findings f : findings) {
            possibleOutcomes.add(f.getName());
        }
        return possibleOutcomes;
    }

    /**
     * Gets the list of probabilities.
     * @return
     */
    public List<Double> getProbabilities() {
        List<Double> probabilities = new LinkedList<>();
        for(Findings f : findings) {
            probabilities.add(f.getProbability());
        }
        return probabilities;
    }

    /**
     * Setter for the probabilities.
     * @param probabilities
     */
    public void setProbabilities(List<Double> probabilities) {
        for(int i = 0; i < findings.size(); i++) {
            findings.get(i).setProbability(probabilities.get(i));
        }
    }

    /**
     * Sets the probability of a specific index.
     * @param index The index of the value to change.
     * @param value The value of the probability.
     */
    public void changeProbability(int index, double value) {
        findings.get(index).setProbability(value);
    }

    /**
     * Sets the probability of a specific index.
     * @param findings The findings to change the probability for.
     * @param value The value of the probability.
     */
    public void changeProbability(String findings, double value) {
        for(Findings f : this.findings) {
            if(f.getName().equals(findings)) {
                this.findings.get(this.findings.indexOf(f)).setProbability(value);
                break;
            }
        }
    }

    /**
     * Gets the element of the possible outcomes list with the given index.
     * @param index The index of the element to get.
     * @return The outcome with the given index.
     */
    public String get(int index) {
        return index>0?findings.get(index).getName():"";
    }

    /**
     * Returns the index of the given String in the list of possible outcomes.
     * @param outcome
     * @return The index or -1 if not found.
     */
    public int indexOf(String outcome) {
        int index = -1;
        for(int i = 0; i < findings.size(); i++) {
            if(findings.get(i).getName().equals(outcome)) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * Gets the size of the list of possible outcomes.
     * @return
     */
    public int size() {
        return findings.size();
    }

    /**
     *  Returns the list of findings.
     * @return
     */
    public LinkedList<Findings> getFindings() {
        return findings;
    }

    @Override
    public String toString() {
        String str = "";
        for(Findings f : findings) {
            str += f.getName() + "; ";
        }
        //remove the last "; "
        str = str.substring(0, str.length() - 2);
        return str;
    }
}

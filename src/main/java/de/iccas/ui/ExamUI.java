package de.iccas.ui;

import com.vaadin.annotations.AutoGenerated;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;

/**
 * !! DO NOT EDIT THIS FILE !!
 * <p>
 * This class is generated by Vaadin Designer and will be overwritten.
 * <p>
 * Please make a subclass with logic and additional interfaces as needed,
 * e.g class LoginView extends LoginDesign implements View { }
 */
@DesignRoot
@AutoGenerated
@SuppressWarnings("serial")
public class ExamUI extends VerticalLayout {
    protected HorizontalLayout hBoxHeader;
    protected ComboBox<String> cbWhere;
    protected Label lbExamGroup;
    protected DateField dfDate;
    protected Grid<ExaminationItem> gridExams;
    protected Label lbColorLegend;
    protected HorizontalLayout hBoxToolbar;
    protected HorizontalLayout hBoxFooter;
    protected Label lbPatientID;
    protected Button btJump;
    protected Button btOK;
    protected Button btCancel;

    public ExamUI() {
        Design.read(this);
    }
}

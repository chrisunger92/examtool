package de.iccas.ui;

import com.vaadin.annotations.AutoGenerated;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;

/**
 * !! DO NOT EDIT THIS FILE !!
 * <p>
 * This class is generated by Vaadin Designer and will be overwritten.
 * <p>
 * Please make a subclass with logic and additional interfaces as needed,
 * e.g class LoginView extends LoginDesign implements View { }
 */
@DesignRoot
@AutoGenerated
@SuppressWarnings("serial")
public class NeckUI extends VerticalLayout {
    protected Label lbHeading;
    protected GridLayout gridLabelsTop;
    protected Label lbNumPatients;
    protected Label lbCurrentPatient;
    protected Label lbDate;
    protected GridLayout gridButtons;
    protected Button btPET;
    protected ProgressBar pbPET;
    protected Button btClinicalExam;
    protected ProgressBar pbClinicalExam;
    protected Button btEndo;
    protected ProgressBar pbEndo;
    protected Button btIntraOp;
    protected ProgressBar pbIntraOp;
    protected Button btCT;
    protected ProgressBar pbCT;
    protected Button btMRI;
    protected ProgressBar pbMRI;
    protected Button btSono;
    protected ProgressBar pbSono;
    protected Button btPatho;
    protected ProgressBar pbPatho;
//    protected Button btPalpation;
//    protected ProgressBar pbPalpation;
    protected Button btGuidedPunction;
    protected ProgressBar pbGuidedPunction;
    protected Button btOthers;
    protected ProgressBar pbOthers;
    protected VerticalLayout lbTotalProgress;
    protected ProgressBar pbTotal;
    protected HorizontalLayout hBoxNavButtons;
    protected Button btPrev;
    protected Button btPrint;
    protected Button btNext;
    protected HorizontalLayout hBoxLabelsBottom;
    protected Label lbPatientID;
    protected Button btSearch;
    protected Button btList;
    protected TextField tfLastName;
    protected TextField tfFirstName;
    protected DateField dfBirthDate;
    protected Image logo;

    public NeckUI() {
        Design.read(this);
    }
}

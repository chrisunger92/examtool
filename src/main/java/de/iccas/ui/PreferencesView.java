package de.iccas.ui;

import com.vaadin.navigator.View;
import com.vaadin.server.FileResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import de.iccas.Main;
import de.iccas.backend.util.Preferences;
import org.vaadin.dialogs.ConfirmDialog;

import java.io.*;

/**
 * A view for editing the preferences with an UI instead of the file...
 */
public class PreferencesView extends VerticalLayout implements View {

    private Main main;

    private Window window;

    private GridLayout contentGrid;

    private final Label lbGenieFile = new Label("GeNIe file");

    private TextField tfGenieFile = new TextField();

    //TODO could lead to the BN manager in the future...
    private Button btSearchGenieFile = new Button("Upload...");

    private final Label lbEnableLogging = new Label("Enable logging");

    private CheckBox cbLogging = new CheckBox();

    private final Label lbLogFile = new Label("Log file");

    private TextField tfLogFile = new TextField();

//    private Button btSearchLogFile = new Button("Search...");

    private final Label lbUseDb = new Label("Use database");

    private CheckBox cbUseDb = new CheckBox();

    private final Label lbDbURL = new Label("Database URL");

    private TextField tfDbURL = new TextField();

    private final Label lbDbUser = new Label("Database user");

    private TextField tfDbUser = new TextField();

    private final Label lbDbPw = new Label("Database password");

    private PasswordField pfDbPw = new PasswordField();

    private final Label lbFakeMode = new Label("Fake mode");

    private CheckBox cbUseFakeMode = new CheckBox();

    private Button btOk = new Button("OK");

    private Button btCancel = new Button("Cancel");

    private Upload genieFileUploader;

    ProgressBar uploadProgress = new ProgressBar();

    private Window uploadWindow;

    /**
     * Constructor.
     * @param mainInstance The instance of Main.
     */
    public PreferencesView(Main mainInstance) {
        main = mainInstance;
        initLayout();
        //add the components to the main layout
        //TODO relative values...
        contentGrid.setWidth("700px");
        addComponents(contentGrid);
        window = new Window("Preferences");
        window.setContent(this);
        window.center();
        main.getUI().addWindow(window);
    }

    /**
     * Initializes the layout / content of the window.
     */
    private void initLayout() {
        contentGrid = new GridLayout(2,13);
        contentGrid.setSpacing(true);
//        contentGrid.setMargin(true);
        contentGrid.setColumnExpandRatio(0,0.3f);
//        contentGrid.setColumnExpandRatio(1,0.7f);

        contentGrid.addComponent(new Label("<h3>General</h3>", ContentMode.HTML),
                0,0,1,0);

        contentGrid.addComponent(lbGenieFile, 0, 1);
        HorizontalLayout genieFileBox = new HorizontalLayout();
        genieFileBox.addComponents(tfGenieFile, btSearchGenieFile);
        contentGrid.addComponent(genieFileBox, 1,1);
        tfGenieFile.setValue(main.preferences.getGenieFile().getAbsolutePath());
        btSearchGenieFile.addClickListener(e -> {
            //open file upload dialog and file chooser
            uploadWindow = new Window("Upload GeNIe file here");
            VerticalLayout uploadLayout = new VerticalLayout();
            UploadReceiver receiver = new UploadReceiver();
            genieFileUploader = new Upload("", receiver);
            genieFileUploader.addSucceededListener(receiver);
            genieFileUploader.setButtonCaption("Choose file...");
            genieFileUploader.addProgressListener(receiver);
            uploadLayout.setWidth("400px");
            uploadLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
            uploadProgress.setCaption("Upload progress: ");
            uploadLayout.addComponents(genieFileUploader, uploadProgress);
            uploadWindow.setContent(uploadLayout);
            uploadWindow.center();
            main.getUI().addWindow(uploadWindow);
        });

        lbFakeMode.setDescription("Enables anonymous use of the tool, as the patient name and date of birth are " +
                "generated with fake data. Disable this, if you want to enter real patient information.");
        contentGrid.addComponent(lbFakeMode,0,2);
        contentGrid.addComponent(cbUseFakeMode,1,2);
        cbUseFakeMode.setValue(main.preferences.usesFakeMode());


        //add a seperator below TODO no line is showing
        contentGrid.addComponent(new Label("<h3>Logging</h3>", ContentMode.HTML),
                0,3,1,3);

        contentGrid.addComponent(lbEnableLogging, 0,4);
        contentGrid.addComponent(cbLogging,1,4);
        cbLogging.setValue(main.preferences.isLoggingEnabled());

        contentGrid.addComponent(lbLogFile, 0, 5);
        HorizontalLayout logFileBox = new HorizontalLayout();
        logFileBox.addComponents(tfLogFile/*, btSearchLogFile*/);
        contentGrid.addComponent(logFileBox,1,5);
        tfLogFile.setValue(main.preferences.getLogFile().getAbsolutePath());
//        btSearchLogFile.addClickListener(e -> {
//
//        });
        //add a seperator below TODO no line is showing
        contentGrid.addComponent(new Label("<h3>Database</h3>", ContentMode.HTML),
                0,6,1,6);

        contentGrid.addComponent(lbUseDb,0,7);
        contentGrid.addComponent(cbUseDb,1,7);
        cbUseDb.setValue(main.preferences.usesDataBase());

        contentGrid.addComponent(lbDbURL,0,8);
        contentGrid.addComponent(tfDbURL,1,8);
        tfDbURL.setValue(main.preferences.getDataBaseURL()!=null?main.preferences.getDataBaseURL():"none");

        contentGrid.addComponent(lbDbUser,0,9);
        contentGrid.addComponent(tfDbUser,1,9);
        tfDbUser.setValue(main.preferences.getDatabaseUser()!=null?main.preferences.getDatabaseUser():"none");

        contentGrid.addComponent(lbDbPw,0,10);
        contentGrid.addComponent(pfDbPw,1,10);
        pfDbPw.setValue(main.preferences.getDatabasePW()!=null?main.preferences.getDatabasePW():"none");

        HorizontalLayout buttonBox = new HorizontalLayout();
        buttonBox.setDefaultComponentAlignment(Alignment.MIDDLE_RIGHT);
        buttonBox.addComponents(btOk,btCancel);
        buttonBox.setSizeFull();
        buttonBox.setSpacing(false);
        btOk.addClickListener(e -> {
            //save preferences and close window
            save();
            window.close();
        });
        btCancel.addClickListener(e -> {
            //show confirmation dialog (if anything was changed) and close window if confirmed
            ConfirmDialog.show(main.getUI(), "Please confirm...",
                    "Are you sure you want to cancel? This means any changes will be lost.",
                    "Yes, cancel",
                    "No, stay on this page",
                    new ConfirmDialog.Listener() {
                        @Override
                        public void onClose(ConfirmDialog confirmDialog) {
                            if(confirmDialog.isConfirmed()) {
                                //go back
                                window.close();
                            } else {
                                //close confirmation dialog and stay on the page
                            }
                        }
                    }
            );
        });
        contentGrid.addComponent(buttonBox,1,12);
    }

    /**
     * Save the input to preferences and file.
     */
    private void save() {
        main.preferences.setGenieFile(new File(tfGenieFile.getValue()));
        main.preferences.setLoggingEnabled(cbLogging.getValue());
        main.preferences.setLogFile(new File(tfLogFile.getValue()));
        main.preferences.setUseDataBase(cbUseDb.getValue());
        main.preferences.setDataBaseURL(tfDbURL.getValue());
        main.preferences.setDataBaseUser(tfDbUser.getValue());
        main.preferences.setDataBasePW(pfDbPw.getValue());
        main.preferences.setFakeMode(cbUseFakeMode.getValue());

        main.preferences.savePreferences();
    }

    /**
     * Upload handler
     */
    public class UploadReceiver implements Upload.Receiver, Upload.SucceededListener, Upload.ProgressListener {
//        private static final long serialVersionUID = 2215337036540966711L;
        OutputStream outputFile = null;
        File file=null;
        @Override
        public OutputStream receiveUpload(String strFilename, String strMIMEType) {
            try {
                file = new File(Preferences.UPLOAD_PATH + "/" + strFilename);
                if(!file.exists()) {
                    file.createNewFile();
                }
                outputFile =  new FileOutputStream(file);
            } catch (IOException e) {
                main.log.error("Couldn't find specified upload path: " + file.getAbsolutePath());
                e.printStackTrace();
            }
            return outputFile;
        }

        protected void finalize() {
            try {
                super.finalize();
                if(outputFile!=null) {
                    outputFile.close();
                }
            } catch (Throwable exception) {
                exception.printStackTrace();
            }
        }

        @Override
        public void uploadSucceeded(Upload.SucceededEvent event) {
            tfGenieFile.setValue(file.getPath());
            uploadWindow.close();
        }

        @Override
        public void updateProgress(final long readBytes, final long contentLength) {
            // this method gets called several times during the update
            uploadProgress.setValue(readBytes / (float) contentLength);
        }
    }
}
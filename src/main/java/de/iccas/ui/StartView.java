package de.iccas.ui;

import com.vaadin.navigator.View;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import de.iccas.Main;

import java.io.File;

/**
 * The start view of the app. This is the first thing the user will see.
 * @author Chris Unger, ICCAS, 2018
 */
public class StartView extends StartUI implements View {

    private Main main;

    /**
     * Constructor.
     */
    public StartView(Main instance) {
        main = instance;

        String basePath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();

        //TODO think of some different design -> setting gender here isn't the best way
        btNeckMale.addClickListener(e -> {
            main.navigator.navigateTo(Main.MAINVIEW);
        });
        btNeckFemale.addClickListener(e -> {
            main.navigator.navigateTo(Main.MAINVIEW);
        });
        btHelp.addClickListener(e -> {
            Window window = new Window("Help");
            Panel content = new Panel();
            VerticalLayout layout = new VerticalLayout();
            //TODO update help according to redesigned UI & funtionality
            //TODO FIX: Images are not shown.
            BrowserFrame browser = new BrowserFrame("",
                    new FileResource(new File(basePath + "/help/help.html")));
            browser.setWidth("800px");
            browser.setHeight("600px");
            layout.addComponent(browser);
            content.setContent(layout);
            window.setContent(content);
            window.center();
            main.getUI().addWindow(window);
        });
        btPref.addClickListener(e -> {
            new PreferencesView(main);
        });
    }
}
